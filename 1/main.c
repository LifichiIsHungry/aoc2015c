#include <stdio.h>
#include <stdlib.h>

#define LOG(s) printf("%s\n", s)
#define INPUT_FILE_SIZE 7000

char* read_file(const char* filename) {
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        LOG("Nie udało się otworzyć pliku do odczytu");
        return NULL;
    }

    char* buffer = malloc(INPUT_FILE_SIZE);
    if (buffer == NULL) {
        LOG("Nie przydzielono poprawnie pamięci");
        return NULL;
    }

    for (int i = 0; i < INPUT_FILE_SIZE; i++)
        buffer[i] = '\0';

    int bytes = fread(buffer, 1, INPUT_FILE_SIZE, fp);
    if (bytes == -1) {
        LOG("Nie udało się odczytać z pliku danych");
        return NULL;
    }

    fclose(fp);
    return buffer;
}

void part1(char *buffer) {
    int floor = 0;
    char* character = &buffer[0];
    while(*character != '\0') {
        switch (*character) {
            case '(': floor++; break;
            case ')': floor--; break;
        }
        character++;
    }

    printf("Piętro: %i\n", floor);
}

void part2(char *buffer) {
    int floor = 0;
    for (int i = 0; i < INPUT_FILE_SIZE; i++) {
        char character = buffer[i];
        switch (character) {
            case '(': floor++; break;
            case ')': floor--; break;
        }

        if (floor == -1) {
            printf("Święty Mikołaj dotarł na -1 piętro w %i kroku\n", i + 1);
            return;
        }
    }

    printf("Święty Mikołaj nie dotarł na -1 piętro");
}

int main(int args, char* argv[]) {
    char* filename = NULL;
    if (args < 2) {
        LOG("Podaj nazwę pliku");
        return 1;
    }
    filename = argv[1];

    char* buffer = read_file(filename);
    if (buffer == NULL) {
        return 1;
    }

    part1(buffer);
    part2(buffer);

    free(buffer);
    return 0;
}
