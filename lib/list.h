#ifndef LIST_H
#define LIST_H

#define ITER_SOME 0
#define ITER_END  1

typedef struct {
    void *value;
    void *next;
} ListElement;

typedef struct {
    ListElement *first;
    size_t count;
} List;

typedef struct {
    List* list;
    ListElement *current;
} Iterator;

/* Create empty list */
List* create_list() {
    List *list = malloc(sizeof(List));
    if (list == NULL) return NULL;

    list->first = NULL;
    list->count = 0;
    return list;
}

/* Return a pointer to the last element stored in the list */
ListElement* get_last_element(List* list) {
    if (list->first == NULL) {
        return NULL;
    }

    ListElement *current_element = list->first;
    while(current_element->next != NULL) {
        current_element = (ListElement*)current_element->next;
    }

    return current_element;
}

/* Free every element in a list and the list itself.
   Pointer `*list` is set to NULL after deallocation. */
void free_list(List *list) {
    if (list == NULL) {
        return;
    }

    ListElement* current_element = list->first;
    while(current_element != NULL) {
        ListElement *next_element = (ListElement*)current_element->next;
        free(current_element->value);
        free(current_element);
        current_element = next_element;
    }

    free(list);
    list = NULL;
}

/* Append value to the list */
int append_to_list(List *list, void *value) {
    ListElement *new_element = malloc(sizeof(ListElement));
    if (new_element == NULL) return 1;

    new_element->value = value;
    new_element->next = NULL;

    ListElement *last_element = get_last_element(list);
    if (last_element == NULL) {
        list->first = new_element;
        list->count = 1;
    } else {
        last_element->next = new_element;
        list->count++;
    }

    return 0;
}

Iterator create_iter(List *list) {
    Iterator iter;
    iter.list = list;
    iter.current = list->first;
    return iter;
}

int iter_next(void* value, Iterator *iter) {
    if (iter->current == NULL)
        return ITER_END;

    ListElement *current = iter->current;
    iter->current = current->next;
    value = current->value;
    return ITER_SOME;
}

#endif /* LIST_H */