#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>

#include "list.h"

#define OK      0
#define ERROR   1

typedef struct {
    unsigned length;
    unsigned width;
    unsigned height;
} Box;

int parse_input(char *filename, List *boxes) {
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("Nie udało się otworzyć pliku");
        return ERROR;
    }

    char *line = NULL;
    size_t n = 0;
    while(getline(&line, &n, fp) != -1) {
        Box *box = malloc(sizeof(Box));
        if (box == NULL) {
            perror("Nie udało się zaalokować pamięci na nową strukturę");
            fclose(fp);
            return ERROR;
        }

        if (sscanf(line, "%ix%ix%i", &box->length, &box->width, &box->height) != 3) {
            printf("Nie udało się przyrównać linii \"%s\"", line);
            fclose(fp);
            return ERROR;
        }

        append_to_list(boxes, box);
    }

    free(line);
    fclose(fp);

    return OK;
}

int box_area(Box *box) {
    return
        2 * box->length * box->width  +
        2 * box->width  * box->height +
        2 * box->height * box->length;
}

int box_volume(Box *box) {
    return box->length * box->width * box->height;
}

int box_smallest_sides_area(Box *box) {
    int smallest_size = (int)INFINITY;

    if ((box->length * box->width) < smallest_size)
        smallest_size = box->length * box->width;

    if ((box->width  * box->height) < smallest_size)
        smallest_size = box->width  * box->height;

    if ((box->height * box->length) < smallest_size)
        smallest_size = box->height * box->length;

    return smallest_size;
}

void part1(List *boxes) {
    int required_sqrt_feet = 0;
    Iterator boxes_iter = create_iter(boxes);
    Box *box = iter_next(&boxes_iter);
    while(box != NULL) {
        required_sqrt_feet += box_area(box) + box_smallest_sides_area(box);
        box = iter_next(&boxes_iter);
    }
    printf("Ilość papieru do zamówienia: %i ft2\n", required_sqrt_feet);
}

int aoc_2_compare_func_bigger(const void *a, const void *b) {
    /* If a is bigger than b, the return value will be bigger than 0 */
    return *(int*)a - *(int*)b;
}

void part2(List *boxes) {
    int required_ribbon = 0;
    Iterator iter = create_iter(boxes);
    Box *box = iter_next(&iter);
    while(box != NULL) {
        int sum_of_distances[3] = {
            box->length + box->length,
            box->width  + box->width,
            box->height + box->height
        };

        qsort(&sum_of_distances, 3, sizeof(int), &aoc_2_compare_func_bigger);

        // printf(
        //     "[\t%i,\t%i,\t%i\t]\n",
        //     sum_of_distances[0],
        //     sum_of_distances[1],
        //     sum_of_distances[2]
        // );

        int smallest_distance = sum_of_distances[0] + sum_of_distances[1];
        required_ribbon += smallest_distance + box_volume(box);

        box = iter_next(&iter);
    }

    printf("Wymagana długość wstążki: %i ft\n", required_ribbon);
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Nie podano nazwy pliku\n");
        return ERROR;
    }
    char *filename = argv[1];

    List *boxes = create_list();
    if (parse_input(filename, boxes) != OK) {
        /* There might be some boxes inserted
           by the function in the list.
           They must be cleaned anyway. */
        free_list(boxes);
        return ERROR;
    }
    printf("Liczba prezentów: %li\n", boxes->count);

    part1(boxes);
    part2(boxes);

    free_list(boxes);

    return OK;
}